<?php
include"header1.php";
?>

      <div class="content-page hospi-content">
<div class="container-fluid container-md">
    <div class="row">
        <div class="col-lg-12 mb-3">
            <div class="d-flex align-items-center justify-content-between welcome-content">
                <div class="navbar-breadcrumb">
                    <h4 class="mb-1">Welcome To Dashboard</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-3 col-sm-6">
            <div class="card card-block card-stretch card-height">
                <div class="card-body">
                    <div class="top-block-one text-center">
                        <div class="text-danger icon mm-icon-box-2 d-block mx-auto rounded">
                            <svg class="svg-icon svg-danger fill-none" id="d-2-1" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                            </svg>
                        </div>
                        <div class="mt-4">
                            <h2><b>76.64k</b></h2>
                            <p class="mb-1">Unique Visitors</p>
                            <span class="text-danger"><i class="ri-arrow-down-s-fill"></i> - 142.73%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 col-sm-6">
            <div class="card card-block card-stretch card-height">
                <div class="card-body">
                    <div class="top-block-one text-center">
                        <div class="text-success icon mm-icon-box-2 d-block mx-auto rounded">
                            <svg class="svg-icon svg-success fill-none" id="d-2-2" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M12 11c0 3.517-1.009 6.799-2.753 9.571m-3.44-2.04l.054-.09A13.916 13.916 0 008 11a4 4 0 118 0c0 1.017-.07 2.019-.203 3m-2.118 6.844A21.88 21.88 0 0015.171 17m3.839 1.132c.645-2.266.99-4.659.99-7.132A8 8 0 008 4.07M3 15.364c.64-1.319 1-2.8 1-4.364 0-1.457.39-2.823 1.07-4" />
                            </svg>
                        </div>
                        <div class="mt-4">
                            <h2 class=""><b>9.67k</b></h2>
                            <p class="mb-1">Impressions</p>
                            <span class="text-success"><i class="ri-arrow-up-s-fill"></i> +82.73%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 col-sm-6">
            <div class="card card-block card-stretch card-height">
                <div class="card-body">
                    <div class="top-block-one text-center">
                        <div class="text-primary icon mm-icon-box-2 d-block mx-auto rounded">
                            <svg class="svg-icon svg-primary fill-none" id="d-2-3" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9" />
                            </svg>
                        </div>
                        <div class="mt-4">
                            <h2><b>83.21k</b></h2>
                            <p class="mb-1">Reach</p>
                            <span class="text-danger"><i class="ri-arrow-down-s-fill"></i>-42.96%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 col-sm-6">
            <div class="card card-block card-stretch card-height">
                <div class="card-body">
                    <div class="top-block-one text-center">
                        <div class=" icon mm-icon-box-2 d-block mx-auto text-warning rounded">
                            <svg class="svg-icon svg-warning fill-none" id="d-2-4" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z" />
                            </svg>
                        </div>
                        <div class="mt-4">
                            <h2><b>18.5%</b></h2>
                            <p class="mb-1">Engagement rate</p>
                            <span class="text-success"><i class="ri-arrow-up-s-fill"></i>+842.73%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-3 col-sm-6">
            <div class="card card-block card-stretch card-height">
                <div class="card-body">
                    <div class="top-block-one text-center">
                        <div class="text-danger icon mm-icon-box-2 d-block mx-auto rounded">
                            <svg class="svg-icon svg-danger fill-none" id="d-2-1" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                            </svg>
                        </div>
                        <div class="mt-4">
                            <h2><b>76.64k</b></h2>
                            <p class="mb-1">Unique Visitors</p>
                            <span class="text-danger"><i class="ri-arrow-down-s-fill"></i> - 142.73%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 col-sm-6">
            <div class="card card-block card-stretch card-height">
                <div class="card-body">
                    <div class="top-block-one text-center">
                        <div class="text-success icon mm-icon-box-2 d-block mx-auto rounded">
                            <svg class="svg-icon svg-success fill-none" id="d-2-2" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M12 11c0 3.517-1.009 6.799-2.753 9.571m-3.44-2.04l.054-.09A13.916 13.916 0 008 11a4 4 0 118 0c0 1.017-.07 2.019-.203 3m-2.118 6.844A21.88 21.88 0 0015.171 17m3.839 1.132c.645-2.266.99-4.659.99-7.132A8 8 0 008 4.07M3 15.364c.64-1.319 1-2.8 1-4.364 0-1.457.39-2.823 1.07-4" />
                            </svg>
                        </div>
                        <div class="mt-4">
                            <h2 class=""><b>9.67k</b></h2>
                            <p class="mb-1">Impressions</p>
                            <span class="text-success"><i class="ri-arrow-up-s-fill"></i> +82.73%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 col-sm-6">
            <div class="card card-block card-stretch card-height">
                <div class="card-body">
                    <div class="top-block-one text-center">
                        <div class="text-primary icon mm-icon-box-2 d-block mx-auto rounded">
                            <svg class="svg-icon svg-primary fill-none" id="d-2-3" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9" />
                            </svg>
                        </div>
                        <div class="mt-4">
                            <h2><b>83.21k</b></h2>
                            <p class="mb-1">Reach</p>
                            <span class="text-danger"><i class="ri-arrow-down-s-fill"></i>-42.96%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 col-sm-6">
            <div class="card card-block card-stretch card-height">
                <div class="card-body">
                    <div class="top-block-one text-center">
                        <div class=" icon mm-icon-box-2 d-block mx-auto text-warning rounded">
                            <svg class="svg-icon svg-warning fill-none" id="d-2-4" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z" />
                            </svg>
                        </div>
                        <div class="mt-4">
                            <h2><b>18.5%</b></h2>
                            <p class="mb-1">Engagement rate</p>
                            <span class="text-success"><i class="ri-arrow-up-s-fill"></i>+842.73%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-block card-stretch card-height">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Top 10 Keywords By Clicks</h4>
                    </div>
                    <div class="card-header-toolbar d-flex align-items-center">
                        <a href="#" class="btn btn-sm btn-outline-primary view-more">View More</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table mb-0 table-bordered data-tables">
                            <thead class="light">
                                <tr>
                                    <th scope="col">Keyword</th>
                                    <th scope="col">Clicks</th>
                                    <th scope="col">Imperssions</th>
                                    <th scope="col">CTR</th>
                                    <th scope="col" class="w-10">Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Keyword 1</td>
                                    <td>45332</td>
                                    <td>927937</td>
                                    <td>3.1</td>
                                    <td><span class="badge badge-success">9.43%</span></td>
                                </tr>
                                <tr>
                                    <td>Keyword 2</td>
                                    <td>3343</td>
                                    <td>465547</td>
                                    <td>5.3</td>
                                    <td><span class="badge badge-info">5.39%</span></td>
                                </tr>
                                <tr class="bg-primary-light">
                                    <td>Keyword 3</td>
                                    <td>5645</td>
                                    <td>465547</td>
                                    <td>2.6</td>
                                    <td><span class="badge badge-warning">7.43%</span></td>
                                </tr>
                                <tr>
                                    <td>Keyword 4</td>
                                    <td>12124</td>
                                    <td>45646</td>
                                    <td>9.1</td>
                                    <td><span class="badge badge-success">9.99%</span></td>
                                </tr>
                                <tr>
                                    <td>Keyword 5</td>
                                    <td>6756</td>
                                    <td>243534</td>
                                    <td>2.4</td>
                                    <td><span class="badge badge-danger">4.97%</span></td>
                                </tr>
                                <tr>
                                    <td>Keyword 6</td>
                                    <td>45332</td>
                                    <td>927937</td>
                                    <td>3.1</td>
                                    <td><span class="badge badge-success">9.43%</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card bg-primary">
                <div class="card-header">
                    <div class="header-title">
                        <h4 class="card-title text-white">Countrywise Impressions</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div id="world-map-gdp1" class="vactormap-height"></div>
                </div>
            </div>
            <!-- <div class="card ">
                <div class="card-header">
                    <div class="header-title">
                        <h4 class="card-title">Goal history</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div class="mm-details">

                        <div class="mm-progress-bar bg-primary-light mt-2">
                            <span class="bg-primary mm-progress progress-1" data-percent="67"
                                style="transition: width 2s ease 0s; width: 67%;">
                                <span class="progress-text-one bg-primary">67%</span>
                            </span>
                        </div>
                        <h5 class="title mt-4">You have reached 70% towards your goal</h5>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>
        </div>
    </div>
   

<?php
include"footer1.php";
?>