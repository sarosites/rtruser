<?php
include"header1.php";
include "db.php"
?>
      <div class="content-page">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12">
               <div class="card">
                  <div class="card-header d-flex justify-content-between">
                     <div class="header-title">
                        <h4 class="card-title">Management</h4>
                     </div>
                  <div class="header-action">
                           <i  type="button" data-toggle="collapse" data-target="#datatable-1" aria-expanded="false" aria-controls="alert-1">
                             <a href="addmanage.php" class="btn btn-outline-dark mt-2 btn-with-icon"><i class="ri-user-line"></i>ADD MANAGEMENT</a>
                           </i>
                        </div>
                  </div>
                  <div class="card-body">
                     <div>
                           <!-- <div class="card"><kbd class="bg-dark"><pre id="bootstrap-datatables" class="text-white"><code>

</code></pre></kbd></div> -->
                        </div>
                     <!-- <p>Images in Bootstrap are made responsive with <code>.img-fluid</code>. <code>max-width: 100%;</code> and <code>height: auto;</code> are applied to the image so that it scales with the parent element.</p> -->
                     <div class="table-responsive">
                        <table id="datatable" class="table data-table table-striped table-bordered" >
                           <thead>
                              <tr>
                                 <th>Name</th>
                                 <th>ID</th>
                                 <th>Email</th>
                                 <th>Phone</th>
                                 
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                         <?php 
                         $ret=mysqli_query($conn,"SELECT * FROM management");
                         $cnt=1;
                         while($row=mysqli_fetch_array($ret))
                        {
                        ?>
                              <tr>
                                 <td><?php echo $row['name'];?></td>
                                 <td><?php echo $row['cid'];?></td>
                                 <td><?php echo $row['email'];?></td>
                                 <td><?php echo $row['phone'];?></td>
                                 
                                 
                                 <td> <a href="management.php?uid=<?php echo $row['id'];?>"> 
                                     <button class="btn btn-primary btn-xs">View</button></a></td>
                              </tr>
                          <?php 
                              $cnt=$cnt+1; 
                           }
                           ?>
                           </tbody>

                        </table>
                     </div>
                  </div>
               </div>
               <a href="dashboard.php" class="btn btn-danger">Back</a>
            </div>
         </div>
      </div>
      </div>

    </div>
    <!-- Wrapper End-->

<?php
include"footer1.php";
?>