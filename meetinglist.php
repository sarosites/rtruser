<?php
include"header1.php";
include "db.php"
?>
      <div class="content-page">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12">
               <div class="card">
                  <div class="card-header d-flex justify-content-between">
                     <div class="header-title">
                        <h4 class="card-title">Meetings</h4>
                     </div>
                     <div class="header-title">
                         <label for="cars">Choose a Filter :</label>
                        <select>
                           <!-- <option>Project</option> -->
                           <option>Meeting</option>
                           <!-- <option>March</option> -->
                        </select>
                        <select>
                          <option>All</option>
                           <option>General Body Meeting</option>
                           <option>Board Meeting</option>
                           <option>Rotary/Rotaract Meeting</option>
                           <!--  <option>International Service</option>
                            <option>DPP</option> -->
                        </select>
                     </div>
                     <div class="header-title">
                         <label for="cars">Choose a Month & Year :</label>
                        <select>
                           <option>jan</option>
                           <option>Feb</option>
                           <option>March</option>
                        </select>
                        <select>
                           <option>2018</option>
                           <option>2019</option>
                           <option>2020</option>
                            <option>2021</option>
                        </select>
                         <a href="#" class="btn-sm btn-outline-dark">Sumbit</a>
                     </div>
                 
                  <div class="header-action">
                           <i  type="button" data-toggle="collapse" data-target="#datatable-1" aria-expanded="false" aria-controls="alert-1">
                             <a href="addmeeting.php" class="btn btn-outline-dark mt-2 btn-with-icon"><i class="ri-user-line"></i>ADD MEETING</a>
                           </i>
                        </div>
                  </div>
                  <div class="card-body">
                     <div>
                           <!-- <div class="card"><kbd class="bg-dark"><pre id="bootstrap-datatables" class="text-white"><code>

</code></pre></kbd></div> -->
                        </div>
                     <!-- <p>Images in Bootstrap are made responsive with <code>.img-fluid</code>. <code>max-width: 100%;</code> and <code>height: auto;</code> are applied to the image so that it scales with the parent element.</p> -->
                     <div class="table-responsive">
                        <table id="datatable" class="table data-table table-striped table-bordered" >
                           <thead>
                              <tr>
                                 <th>Name</th>
                                 
                                 <th>Date</th>
                                 <th>Meeting type</th>
                                 <th>Venue</th>
                                 <th>Time</th>
                                 <th>Purpose</th>
                                 
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                         <?php 
                         $ret=mysqli_query($conn,"SELECT * FROM meeting");
                         $cnt=1;
                         while($row=mysqli_fetch_array($ret))
                        {
                        ?>
                              <tr>
                                 <td><?php echo $row['title'];?></td>
                                 
                                 <td><?php echo $row['mdate'];?></td>
                                 <td><?php echo $row['meetingtype'];?></td>

                                 <td><?php echo $row['venue'];?></td>

                                 <td><?php echo $row['mtime'];?></td>

                                 
                                 <td><?php echo $row['purpose'];?></td>
                                 
                                 <td> <a href="meetingview.php?uid=<?php echo $row['id'];?>"> 
                                     <button class="btn btn-primary btn-xs">View</button></a></td>
                              </tr>
                          <?php 
                              $cnt=$cnt+1; 
                           }
                           ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div><a href="dashboard.php" class="btn btn-danger"> Back</a> 
            </div>
           
         </div>
      </div>
      </div>
    </div>
    <!-- Wrapper End-->

<?php
include"footer1.php";
?>