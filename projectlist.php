<?php
error_reporting(0);
session_start();
if(!isset($_SESSION['name']))
{
echo "<script>window.location.href='log/index.html'</script>";
}
else
{
include"header1.php";

?>
      <div class="content-page">
      <div class="container-fluid">
         <div class="row">
      <div class="col-lg-4 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-danger">
                                    <svg class="svg-icon svg-danger" width="50" height="52" id="h-01"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"
                                            d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"
                                            d="M20.488 10H14V3.512A9.025 9.025 0 0120.488 9z" />
                                    </svg>
                                </div>

                                <div class="mm-cart-text">
                                    <h2 class="font-weight-700">0</h2>
                                    <p class="mb-0 text-danger">On Going</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-success">
                                    <svg class="svg-icon svg-success mr-4" width="50" height="52" id="h-02"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"
                                            d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z" />
                                    </svg>
                                </div>
                                <div class="mm-cart-text">
                                    <h2 class="font-weight-700">0</h2>
                                    <p class="mb-0 text-success">Upcomming</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-primary">
                                    <svg class="svg-icon svg-blue mr-4" id="h-03" width="50" height="52"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                                    </svg>
                                </div>
                                <div class="mm-cart-text">
                                    <h2 class="font-weight-700">0</h2>
                                    <p class="mb-0 text-primary">Completed</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
   </div>
         <div class="row">
            <div class="col-sm-12">

               <div class="card">
                  <div class="card-header d-flex justify-content-between">
                     <div class="header-title">
                        <h4 class="card-title">Projects</h4>
                     </div>
                      <div class="header-title">
                         <label for="cars">Choose a Filter :</label>
                        <select>
                           <option>Project</option>
                           <!-- <option>Meeting</option> -->
                           <!-- <option>March</option> -->
                        </select>
                        <select>
                          <option>All</option>
                           <option value="Club Service">Club Service</option>
                                <option value="Community Service">Community Service</option>
                                <option value="Professional Service">Professional Service</option>
                                <option value="International Service">International Service</option>
                                <option value="District Priority Projects">District Priority Projects</option>
                        </select>
                     </div>
                     <div class="header-title">
                         <label for="cars">Choose a Month & Year :</label>
                        <select>
                           <option>Jan</option>
                           <option>Feb</option>
                           <option>March</option>
                           <option>April</option>
                           <option>May</option>
                           <option>June</option>
                           <option>July</option>
                           <option>Aug</option>
                           <option>Sept</option>
                           <option>Oct</option>
                           <option>Nov</option>
                           <option>Dec</option>
                        </select>
                        <select>
                           
                           <option>2020</option>
                            <option>2021</option>
                        </select>
                         <a href="#" class="btn-sm btn-outline-dark">Sumbit</a>
                     </div>
                  <div class="header-action">
                           <i  type="button" data-toggle="collapse" data-target="#datatable-1" aria-expanded="false" aria-controls="alert-1">
                             <a href="addproject.php" class="btn btn-outline-dark mt-2 btn-with-icon"><i class="ri-user-line"></i>ADD PROJECT</a>
                           </i>
                        </div>
                  </div>
                  <div class="card-body">
                     <div>
                           <!-- <div class="card"><kbd class="bg-dark"><pre id="bootstrap-datatables" class="text-white"><code>

</code></pre></kbd></div> -->
                        </div>
                     <!-- <p>Images in Bootstrap are made responsive with <code>.img-fluid</code>. <code>max-width: 100%;</code> and <code>height: auto;</code> are applied to the image so that it scales with the parent element.</p> -->
                     <div class="table-responsive">
                        <table id="datatable" class="table data-table table-striped table-bordered" >
                           <thead>
                              <tr>
                                 <th>Name</th>
                                 <th>From Date</th>
                                 <th>To date</th>
                                 <th>Avenue</th>
                                 <th>Project with</th>
                                 <th>Venue</th>

                                 <th>Time</th>
                                 <th>Chairman</th>
                                 
                                 
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                         <?php 
                         $ret=mysqli_query($conn,"SELECT * FROM project");
                         $cnt=1;
                         while($row=mysqli_fetch_array($ret))
                        {
                        ?>
                              <tr>
                                 <td><?php echo $row['name'];?></td>
                                 
                                 <td><?php echo $row['pfromdate'];?></td>
                                 <td><?php echo $row['ptodate'];?></td>
                                 <td><?php echo $row['vid'];?></td>
                                 <td><?php echo $row['projectof'];?></td>
                                 <td><?php echo $row['place'];?></td>
                                 <td><?php echo $row['time'];?></td>
                                 <td><?php echo $row['chairmen'];?></td>
                                 
                                 
                                 <td> <a href="viewpillar1.php?uid=<?php echo $row['id'];?>"> 
                                     <button class="btn btn-primary btn-xs">View</button></a></td>
                              </tr>
                          <?php 
                              $cnt=$cnt+1; 
                           }
                           ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
            <a href="dashboard.php" class="btn btn-danger">BACK </a>
            <div id="exampleModalCenteredScrollable" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredScrollableTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h5 class="modal-title" id="exampleModalCenteredScrollableTitle">Project View</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <p>Project Name  : Sundar Yatra</p>
                                 <p>Date AND Time : 19-01-2020 12.30 PM </p>
                                 <p>Venue : Coimbatore</p>
                                 <p>Event Chairman : Amith </p>
                                 <p>Avenue : Community </p>
                                 <p>PosterImage :<img src="assets/images/1.jpg" style="width: 40%;"> </p>
                                 <p>Event Clicks :
                                  <br>
                                  <img src="assets/images/5.jpg" style="width: 60%;">
                                  <br>
                                 <img src="assets/images/6.jpg" style="width: 60%;">
                                 <br>
                                 <img src="assets/images/7.jpg" style="width: 60%;"> </p>
                              </div>
                              <div class="modal-footer">
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 <button type="button" class="btn btn-primary">Save changes</button>
                              </div>
                           </div>
                        </div>
                     </div>
         </div>
      </div>
      </div>
    </div>
   
    <!-- Wrapper End-->

<?php
include"footer1.php";
}
?>