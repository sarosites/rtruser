<?php
include"header1.php";
?>
 <div class="content-page">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12 col-lg-12">
               
               <div class="card">
                  <div class="card-header d-flex justify-content-between">
                     <div class="header-title">
                        <h4 class="card-title">Dashboard / Report / Members</h4>
                     </div>
                  <!-- <div class="header-action">
                           <i  type="button" data-toggle="collapse" data-target="#form-validation-4" aria-expanded="false" aria-controls="alert-1">
                              <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                 <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 20l4-16m4 4l4 4-4 4M6 16l-4-4 4-4" />
                              </svg>
                           </i>
                        </div> -->
                  </div>
                  <div class="card-body">
                     <div class="collapse" id="form-validation-4">
                           <div class="card"><kbd class="bg-dark"><pre id="tooltip" class="text-white"><code>
&#x3C;form class=&#x22;needs-validation&#x22; novalidate&#x3E;
   &#x3C;div class=&#x22;form-row&#x22;&#x3E;
      &#x3C;div class=&#x22;col-md-6 mb-3&#x22;&#x3E;
         &#x3C;label for=&#x22;validationTooltip01&#x22;&#x3E;First name&#x3C;/label&#x3E;
         &#x3C;input type=&#x22;text&#x22; class=&#x22;form-control&#x22; id=&#x22;validationTooltip01&#x22; value=&#x22;Mark&#x22; required&#x3E;
         &#x3C;div class=&#x22;valid-tooltip&#x22;&#x3E;
            Looks good!
         &#x3C;/div&#x3E;
      &#x3C;/div&#x3E;
      &#x3C;div class=&#x22;col-md-6 mb-3&#x22;&#x3E;
         &#x3C;label for=&#x22;validationTooltip02&#x22;&#x3E;Last name&#x3C;/label&#x3E;
         &#x3C;input type=&#x22;text&#x22; class=&#x22;form-control&#x22; id=&#x22;validationTooltip02&#x22; value=&#x22;Tech&#x22; required&#x3E;
         &#x3C;div class=&#x22;valid-tooltip&#x22;&#x3E;
            Looks good!
         &#x3C;/div&#x3E;
      &#x3C;/div&#x3E;
      &#x3C;div class=&#x22;col-md-6 mb-3&#x22;&#x3E;
         &#x3C;label for=&#x22;validationTooltipUsername&#x22;&#x3E;Username&#x3C;/label&#x3E;
         &#x3C;div class=&#x22;input-group&#x22;&#x3E;
            &#x3C;div class=&#x22;input-group-prepend&#x22;&#x3E;
               &#x3C;span class=&#x22;input-group-text&#x22; id=&#x22;validationTooltipUsernamePrepend&#x22;&#x3E;@&#x3C;/span&#x3E;
            &#x3C;/div&#x3E;
            &#x3C;input type=&#x22;text&#x22; class=&#x22;form-control&#x22; id=&#x22;validationTooltipUsername&#x22; aria-describedby=&#x22;validationTooltipUsernamePrepend&#x22; required&#x3E;
            &#x3C;div class=&#x22;invalid-tooltip&#x22;&#x3E;
               Please choose a unique and valid username.
            &#x3C;/div&#x3E;
         &#x3C;/div&#x3E;
      &#x3C;/div&#x3E;
      &#x3C;div class=&#x22;col-md-6 mb-3&#x22;&#x3E;
         &#x3C;label for=&#x22;validationTooltip03&#x22;&#x3E;City&#x3C;/label&#x3E;
         &#x3C;input type=&#x22;text&#x22; class=&#x22;form-control&#x22; id=&#x22;validationTooltip03&#x22; required&#x3E;
         &#x3C;div class=&#x22;invalid-tooltip&#x22;&#x3E;
            Please provide a valid city.
         &#x3C;/div&#x3E;
      &#x3C;/div&#x3E;
      &#x3C;div class=&#x22;col-md-6 mb-3&#x22;&#x3E;
         &#x3C;label for=&#x22;validationTooltip04&#x22;&#x3E;State&#x3C;/label&#x3E;
         &#x3C;select class=&#x22;form-control&#x22; id=&#x22;validationTooltip04&#x22; required&#x3E;
            &#x3C;option selected disabled value=&#x22;&#x22;&#x3E;Choose...&#x3C;/option&#x3E;
            &#x3C;option&#x3E;...&#x3C;/option&#x3E;
         &#x3C;/select&#x3E;
         &#x3C;div class=&#x22;invalid-tooltip&#x22;&#x3E;
            Please select a valid state.
         &#x3C;/div&#x3E;
      &#x3C;/div&#x3E;
      &#x3C;div class=&#x22;col-md-6 mb-3&#x22;&#x3E;
         &#x3C;label for=&#x22;validationTooltip05&#x22;&#x3E;Zip&#x3C;/label&#x3E;
         &#x3C;input type=&#x22;text&#x22; class=&#x22;form-control&#x22; id=&#x22;validationTooltip05&#x22; required&#x3E;
         &#x3C;div class=&#x22;invalid-tooltip&#x22;&#x3E;
            Please provide a valid zip.
         &#x3C;/div&#x3E;
      &#x3C;/div&#x3E;
   &#x3C;/div&#x3E;
   &#x3C;button class=&#x22;btn btn-primary&#x22; type=&#x22;submit&#x22;&#x3E;Submit form&#x3C;/button&#x3E;
&#x3C;/form&#x3E;
</code></pre></kbd></div>
                        </div>
                     <form class="needs-validation" novalidate>
                        <div class="form-row">
                           <div class="col-md-6 mb-3">
                              <label for="validationTooltip01">From Date</label>
                              <input type="date" class="form-control" id="validationTooltip01" value="Mark" required>
                              <div class="invalid-tooltip">
                                 Please enter a date
                              </div>
                           </div>

                            <div class="col-md-6 mb-3">
                              <label for="validationTooltip01">To Date</label>
                              <input type="date" class="form-control" id="validationTooltip01" value="Mark" required>
                              <div class="valid-tooltip">
                                 Please enter a date
                              </div>
                           </div>

                           
                        </div>
                        <button class="btn btn-primary" type="submit">Get Report</button>
                        <a href="report.php" class="btn btn-danger">Back</a>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
    </div>
<?php

include"footer1.php";
?>